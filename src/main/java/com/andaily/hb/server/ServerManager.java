package com.andaily.hb.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * 2024/1/7 23:41
 *
 * @author Shengzhao Li
 * @since 3.0.0
 */
@Component
public class ServerManager {

    private static final Logger LOG = LoggerFactory.getLogger(ServerManager.class);

    @Autowired
    private TcpServer tcpServer;

    @Autowired
    private UdpServer udpServer;


    public ServerManager() {
    }


    @EventListener(ApplicationReadyEvent.class)
    public void start() {

        LOG.info("ServerManager call 'start' ");
        new Thread(() -> {
            tcpServer.start();
        }).start();

        new Thread(() -> {
            udpServer.start();
        }).start();

    }


    @EventListener(ContextClosedEvent.class)
    public void stop() {
        LOG.info("ServerManager call 'stop' ");
        tcpServer.stop();
        udpServer.stop();
    }
}
