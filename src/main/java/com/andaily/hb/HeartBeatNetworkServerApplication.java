package com.andaily.hb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * Spring Boot 启动引导
 *
 * @author Shengzhao Li
 * @since 3.0.0
 */
@SpringBootApplication
public class HeartBeatNetworkServerApplication {


    /**
     * 启动 springboot 入口
     *
     * @param args args
     */
    public static void main(String[] args) {
        SpringApplication.run(HeartBeatNetworkServerApplication.class, args);
    }


}
