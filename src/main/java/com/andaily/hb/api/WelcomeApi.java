package com.andaily.hb.api;

import com.andaily.hb.server.TcpServer;
import com.andaily.hb.server.UdpServer;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 2024/1/7 23:24
 *
 * @author Shengzhao Li
 * @since 3.0.0
 */
@RestController
public class WelcomeApi {


    @Autowired
    private TcpServer tcpServer;

    @Autowired
    private UdpServer udpServer;


    @GetMapping("/")
    public Map<String, Object> welcome() {
        return ImmutableMap.of(
                "message", "Welcome to HeartBeat-Network-Server",
                "tcpServer.port", tcpServer.getPort(),
                "udpServer.port", udpServer.getPort()
        );

    }

}
