package com.andaily.hb.config;

import com.andaily.hb.server.TcpServer;
import com.andaily.hb.server.UdpServer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 2024/1/7 23:31
 *
 * @author Shengzhao Li
 * @since 3.0.0
 */
@Configuration
public class ServerConfig {


    @Value("${tcp.server.port:13456}")
    private int tcpServerPort;

    @Value("${udp.server.port:13458}")
    private int udpServerPort;


    @Bean
    public TcpServer tcpServer() {
        return new TcpServer(tcpServerPort);
    }

    @Bean
    public UdpServer udpServer() {
        return new UdpServer(udpServerPort);
    }

}
